# ics-ans-role-zfs-snapshot-cleanup

Ansible role to install zfs-snapshot-cleanup.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-zfs-snapshot-cleanup
```

## License

BSD 2-clause
